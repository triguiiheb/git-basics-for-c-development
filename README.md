# Git Basics For C Ddevelopment
This README provides instructions on how to build and run the `Git Basics For C Development` project.

## Building the Project

To build the project, you will need CMake and a C compiler. Follow these steps:

1. **Open a terminal and navigate to the project directory.**

2. **Create a build directory and enter it:**

3. **Run CMake to configure the project and generate a Makefile:**

4. **Build the project with Make:**

This will compile the source files and create an executable named `git-basics-for-c-development`.

## Running the Project

After building the project, you can run the executable directly from the build directory:


This will execute the program and display its output. 
If the program is set up to print "Hello, world!" as per the sample code, you should see this message in the terminal.

### Notes

- Ensure that CMake and your C compiler (like gcc or clang) are installed on your system. On Ubuntu, you can install these with:

sudo apt-get update

sudo apt-get install build-essential cmake clang

OR FOR GCC

sudo apt-get update

sudo apt-get install build-essential cmake gcc

