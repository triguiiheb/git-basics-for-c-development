#include <stdio.h>
#include <string.h>
void print_string(const char *str) {
   printf("%s\n", str);
}
int main() {
   printf("Hello, world!\n");
   print_string("This is a test.");
   for (int i = 1; i <= 10; i++) {
       printf("%d\n", i);
   }
   return 0;
}
